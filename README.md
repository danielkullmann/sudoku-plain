# Sudoku in plain JavaScript (well, with jQuery..)

Sorry, I just don't want to live without the convenience to use jQuery..

## TODO list

* Colors should mix correctly:
  * fixed background color (from puzzle definition)
  * selected background color
  * active and selected cells
* Complete puzzle loading code
* "Check" functionality
* Save puzzle state locally in browser
* "Undo" and "redo" functionality
* "Restart" functionality
* Create and edit puzzles
