"use strict";
/* exported make_id parse_id calculate_angle calculate_length get_row_and_col_from_div */

function make_id(row, col) {
    return "c-" + row + "-" + col;
}

function parse_id(id) {
    var aParts = id.split("-");
    return [parseInt(aParts[1]), parseInt(aParts[2])];
}

/* 
  Calculate angle between points (cx, cy) and (ex, ey) 
  https://stackoverflow.com/a/9614122/85615
*/
function calculate_angle(cx, cy, ex, ey) {
    var dy = ey - cy;
    var dx = ex - cx;
    var theta = Math.atan2(dy, dx); // range (-PI, PI]
    theta *= 180 / Math.PI; // rads to degrees, range (-180, 180]
    return theta;
}

function calculate_length(sx, sy, ex, ey, boxWidth) {
    var dx = ex - sx;
    var dy = ey - sy;
    return Math.sqrt(dx * dx + dy * dy)+boxWidth/8;
}

function get_row_and_col_from_div(oDiv) {
    if (!oDiv) {
        return null;
    }
    var aClasses = oDiv.className.split(" ");
    var sCellClass = aClasses.filter((item) => { return item.startsWith("c-"); })[0];
    if (sCellClass === undefined) {
        return get_row_and_col_from_div(oDiv.parentNode);
    }
    return parse_id(sCellClass);
}

