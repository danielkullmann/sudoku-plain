"use strict";
/* globals Sudoku get_row_and_col_from_div */

var sudoku = new Sudoku();

function click_handler(oEvent) {
    var aPosition = get_row_and_col_from_div(oEvent.target);
    sudoku.set_active(aPosition[0], aPosition[1]);
    sudoku.clear_selected();
}

var mouse_clicked = false;

function mousedown_handler(oEvent) {
    var aPosition = get_row_and_col_from_div(oEvent.target);
    sudoku.set_active(aPosition[0], aPosition[1]);
    sudoku.clear_selected();
    mouse_clicked = true;
}

function mouseup_handler() {
    mouse_clicked = false;
}

function mousemouve_handler(oEvent) {
    if (mouse_clicked) {
        var aPosition = get_row_and_col_from_div(oEvent.target);
        if (aPosition) {
            sudoku.extend_selected_pos(aPosition[0], aPosition[1]);
        }
    }
}

function resize_handler() {
    this.sudoku.handle_resize();
}

function load_puzzle(sPuzzleId) {
    var oRegex = /[^0-9a-z]/gi;
    sPuzzleId = sPuzzleId.replace(oRegex, "");
    $.ajax({
        url: "./puzzles/" + sPuzzleId + ".json" + "?t=" + new Date().getTime(),
        success: function (oData) {
            sudoku.load_puzzle(oData);
        },
        error: function () {
            console.log("Could not load puzzle " + sPuzzleId);
            console.log(arguments);
        }
    });
}

function key_handler(oEvent) {
    let ctrlKey = oEvent.ctrlKey;
    let key = oEvent.key;
    if (ctrlKey) {
        switch (key) {
            case "ArrowLeft":
                event.preventDefault();
                sudoku.extend_selected_left();
                break;
            case "ArrowRight":
                event.preventDefault();
                sudoku.extend_selected_right();
                break;
            case "ArrowUp":
                event.preventDefault();
                sudoku.extend_selectedUp();
                break;
            case "ArrowDown":
                event.preventDefault();
                sudoku.extend_selectedDown();
                break;
            case "z":
                event.preventDefault();
                sudoku.undo();
                break;
            case "y":
                event.preventDefault();
                sudoku.redo();
                break;
            case "d":
                event.preventDefault();
                sudoku.clear_content();
                break;
            default:
                break;
        }
    } else {
        switch (key) {
            case "ArrowLeft":
                event.preventDefault();
                sudoku.clear_selected();
                sudoku.move_active_left();
                break;
            case "ArrowRight":
                event.preventDefault();
                sudoku.clear_selected();
                sudoku.move_active_right();
                break;
            case "ArrowUp":
                event.preventDefault();
                sudoku.clear_selected();
                sudoku.move_active_up();
                break;
            case "ArrowDown":
                event.preventDefault();
                sudoku.clear_selected();
                sudoku.move_active_down();
                break;
            case "z": // english keyboard (qwerty)
            case "y": // german keyboard (qwertz)
            case "w": // french keaboard (azerty)
                event.preventDefault();
                sudoku.set_mode(sudoku.MODES.DIGIT);
                break;
            case "x":
                event.preventDefault();
                sudoku.set_mode(sudoku.MODES.CORNER);
                break;
            case "c":
                event.preventDefault();
                sudoku.set_mode(sudoku.MODES.CENTER);
                break;
            case "v":
                event.preventDefault();
                sudoku.set_mode(sudoku.MODES.COLOR);
                break;
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                var digit = parseInt(key);
                sudoku.set_content(digit);
                break;
            case "Backspace":
                sudoku.clear_content();
                break;
            default:
                //console.log("Unknown key " + key);
                break;
        }
    }
}

$().ready(function () {
    document.addEventListener("keydown", key_handler);
    window.addEventListener("resize", resize_handler);
    $(".cell").click(click_handler);
    $(".cell").mousedown(mousedown_handler);
    $(".cell").mouseup(mouseup_handler);
    $(".cell").mousemove(mousemouve_handler);
    sudoku.render_active();
    if (document.location.search.length > 1) {
        sudoku.setup_controls();
        load_puzzle(document.location.search.substring(1));
    }

});
