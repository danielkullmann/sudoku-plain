#!/usr/bin/env python3

import os
import json

result = []

for file in os.listdir(os.path.join(".", "puzzles")):
    if file.endswith(".json"):
        with open(os.path.join(".", "puzzles", file), "r") as fh:
            puzzle = json.load(fh)
            entry = dict()
            entry["id"] = os.path.basename(file)
            entry["title"] = puzzle.get("title", "")
            entry["author"] = puzzle.get("author", "")
            entry["description"] = puzzle.get("description", "")
            result.append(entry)

with open("puzzles.json", "w") as fh:
    json.dump(result, fh)
