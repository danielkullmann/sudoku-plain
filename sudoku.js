"use strict";
/* globals make_id parse_id calculate_angle calculate_length */
/* exported Sudoku */

function Sudoku() {

    // the values in this map correspond to the CSS class of the button
    // to change to this mode
    this.MODES = {
        DIGIT: "mode-normal",
        CORNER: "mode-corner",
        CENTER: "mode-center",
        COLOR: "mode-color"
    };

    this.COLORS = [
        "", // 0 is no color
        "#000000f0", // black
        "#b0b0b0f0", // grey
        "transparent", // "white"
        "#60f060b0", // green
        "#ff00ffb0", // violet
        "#ff3030b0", // red
        "#ff8020b0", // orange
        "#ffff20b0", // yellow
        "#4040ffb0", // blue
    ];
    this.state = {};
    this.state.puzzle = null;
    this.state.row = 1;
    this.state.col = 1;
    this.state.mode = this.MODES.DIGIT;
    this.state.fixed_digits = new Map();
    this.state.digits = new Map();
    this.state.center_checkmarks = new Map(); // id -> [digits]
    this.state.corner_checkmarks = new Map(); // id -> [digits]
    this.state.colors = new Map(); // id -> color number
    this.clear_selected();
}

Sudoku.prototype.setup_controls = function () {
    Object.values(this.MODES).forEach((mode) => {
        $("." + mode).click(() => { this.set_mode(mode); });
    });
    $(".aux-undo").click(() => { this.undo(); });
    $(".aux-redo").click(() => { this.redo(); });
    $(".aux-delete").click(() => { this.clear_selected(); });
    [1,2,3,4,5,6,7,8,9].forEach((number) => {
        $(".number-"+number).click(() => { this.set_content(number); });
    });
};

Sudoku.prototype.set_mode = function (mode) {
    this.state.mode = mode;
    $(".mode-controls>.button").removeClass("selected-button");
    $("." + mode).addClass("selected-button");
    if (mode === this.MODES.COLOR) {
        [1,2,3,4,5,6,7,8,9].forEach((number) => {
            $(".number-"+number).css("background-color", this.COLORS[number]);
            // TODO move the functionality "invert color" to helpers.js
            var color = this.COLORS[number];
            if (color === "transparent") color = "#ffffff";
            var colorNumber = parseInt(color.substring(1, 7), 16);
            var invertedColorNumber = parseInt("ffffff", 16) - colorNumber;
            $(".number-"+number).css("color", Number(invertedColorNumber).toString(16));
        });
    } else {
        [1,2,3,4,5,6,7,8,9].forEach((number) => {
            $(".number-"+number).css("background-color", "");
            $(".number-"+number).css("color", "black");
        });
    }
};

Sudoku.prototype.render_active = function () {
    this.render(this.state.row, this.state.col);
};

Sudoku.prototype.render_all = function () {
    for (var row = 1; row <= 9; row++) {
        for (var col = 1; col <= 9; col++) {
            this.render(row, col);
        }
    }
};

Sudoku.prototype.render = function (row, col) {
    //console.log("render", row, col);
    var id = make_id(row, col);
    var oCell = $("." + id);
    oCell.find(".inner-cell").children().remove();
    var isActive = (this.state.row === row && this.state.col === col);
    var isSelected = this.selected.has(id);
    var fixedDigit = this.state.fixed_digits.get(id);
    var digit = this.state.digits.get(id);
    if (fixedDigit) {
        oCell.find(".inner-cell").text("" + fixedDigit);
        oCell.addClass("fixed-digit");
    } else if (digit) {
        oCell.find(".inner-cell").text("" + digit);
        oCell.removeClass("fixed-digit");
    } else {
        oCell.find(".inner-cell").text("");
        oCell.removeClass("fixed-digit");
        var aCornerCheckmarks = this.state.corner_checkmarks.get(id) || [];
        var aCenterCheckmarks = this.state.center_checkmarks.get(id) || [];
        if (aCenterCheckmarks.length > 0) {
            var oChild = document.createElement("span");
            oChild.className = "cell-center";
            oChild.textContent = aCenterCheckmarks.sort().join("");
            oCell.find(".inner-cell").append(oChild);
        }
        if (aCornerCheckmarks.length > 0) {
            var aCorners = [[], [], [], []];
            aCornerCheckmarks.sort();
            aCornerCheckmarks.forEach((value, index) => {
                aCorners[index % 4].push(value);
            });
            aCorners.forEach((value, index) => {
                if (value.length > 0) {
                    var oChild = document.createElement("span");
                    oChild.className = "cell-corner-" + (index + 1);
                    oChild.textContent = value.join("");
                    oCell.find(".inner-cell").append(oChild);
                }
            });
        }

    }

    if (isActive) {
        oCell.find(".inner-cell").addClass("active");
    } else {
        oCell.find(".inner-cell").removeClass("active");
    }
    if (isSelected) {
        oCell.find(".inner-cell").addClass("selected");
    } else {
        oCell.find(".inner-cell").removeClass("selected");
    }
    if (this.state.colors.has(id)) {
        oCell.find(".color-cell").css("background-color", this.state.colors.get(id));
    } else {
        oCell.find(".color-cell").css("background-color", "");
    }
};

Sudoku.prototype.clear_selected = function () {
    var mOld = new Set(this.selected);
    this.selected = new Set();
    mOld.forEach(function (id) {
        var aPosition = parse_id(id);
        this.render(aPosition[0], aPosition[1]);
    }.bind(this));
};

Sudoku.prototype.toggle_center_checkmark = function (row, col, digit) {
    var id = make_id(row, col);
    var marks = [];
    if (this.state.center_checkmarks.has(id)) {
        marks = this.state.center_checkmarks.get(id);
    }
    if (marks.find((v) => v === digit)) {
        marks = marks.filter((v) => v !== digit);
    } else {
        marks.push(digit);
    }
    this.state.center_checkmarks.set(id, marks);
    this.render(row, col);
};

Sudoku.prototype.toggle_corner_checkmark = function (row, col, digit) {
    var id = make_id(row, col);
    var marks = [];
    if (this.state.corner_checkmarks.has(id)) {
        marks = this.state.corner_checkmarks.get(id);
    }
    if (marks.find((v) => v === digit)) {
        marks = marks.filter((v) => v !== digit);
    } else {
        marks.push(digit);
    }
    this.state.corner_checkmarks.set(id, marks);
    this.render(row, col);
};

Sudoku.prototype.set_color = function (row, col, digit) {
    var id = make_id(row, col);
    this.state.colors.set(id, this.COLORS[digit]);
};

Sudoku.prototype.set_position_content = function (row, col, digit) {
    switch (this.state.mode) {
        case this.MODES.DIGIT:
            this.state.digits.set(make_id(row, col), digit);
            break;
        case this.MODES.CENTER:
            this.toggle_center_checkmark(row, col, digit);
            break;
        case this.MODES.CORNER:
            this.toggle_corner_checkmark(row, col, digit);
            break;
        case this.MODES.COLOR:
            this.set_color(row, col, digit);
            break;
        default:
            throw "Unknown mode " + this.state.mode;
    }
    this.render(row, col);
};

Sudoku.prototype.set_content = function (digit) {
    if (this.selected.size == 0) {
        this.set_position_content(this.state.row, this.state.col, digit);
    } else {
        this.selected.forEach(function (id) {
            var aPosition = parse_id(id);
            this.set_position_content(aPosition[0], aPosition[1], digit);
        }.bind(this));
    }
};

Sudoku.prototype.clear_position_content = function (row, col) {
    this.state.digits.delete(make_id(row, col));
    this.state.corner_checkmarks.delete(make_id(row, col));
    this.state.center_checkmarks.delete(make_id(row, col));
    this.render(row, col);
};

Sudoku.prototype.clear_content = function () {
    if (this.selected.size == 0) {
        this.clear_position_content(this.state.row, this.state.col);
    } else {
        this.selected.forEach(function (id) {
            var aPosition = parse_id(id);
            this.clear_position_content(aPosition[0], aPosition[1]);
        }.bind(this));
    }
};

Sudoku.prototype.set_active = function (row, col) {
    if (this.state.row === row && this.state.col === col) {
        return;
    }
    var oldRow = this.state.row;
    var oldCol = this.state.col;
    this.state.row = row;
    this.state.col = col;
    this.render(oldRow, oldCol);
    this.render(this.state.row, this.state.col);
    // $(".cell").removeClass("active");
    // $("." + make_id(row, col)).addClass("active");
    //console.log("set active " + row + " " + col);
};

Sudoku.prototype.extend_selectedUp = function () {
    this.extend_selected(this.move_active_up.bind(this));
};

Sudoku.prototype.extend_selectedDown = function () {
    this.extend_selected(this.move_active_down.bind(this));
};

Sudoku.prototype.extend_selected_left = function () {
    this.extend_selected(this.move_active_left.bind(this));
};

Sudoku.prototype.extend_selected_right = function () {
    this.extend_selected(this.move_active_right.bind(this));
};

Sudoku.prototype.extend_selected = function (moveMethod) {
    var oldRow = this.state.row;
    var oldCol = this.state.col;
    moveMethod();
    //console.log("extend selected: ", oldRow, oldCol, this.state.row, this.state.col, this.selected);
    this.selected.add(make_id(oldRow, oldCol));
    this.selected.add(make_id(this.state.row, this.state.col));
    this.render(oldRow, oldCol);
    this.render_active();
};

Sudoku.prototype.extend_selected_pos = function (row, col) {
    //console.log("extend selected: ", oldRow, oldCol, this.state.row, this.state.col, this.selected);
    this.state.row = row;
    this.state.col = col;
    this.selected.add(make_id(this.state.row, this.state.col));
    this.render_active();
};

Sudoku.prototype.move_active_right = function () {
    if (this.state.col < 9) {
        this.set_active(this.state.row, this.state.col + 1);
    }
};

Sudoku.prototype.move_active_left = function () {
    if (this.state.col > 1) {
        this.set_active(this.state.row, this.state.col - 1);
    }
};

Sudoku.prototype.move_active_up = function () {
    if (this.state.row > 1) {
        this.set_active(this.state.row - 1, this.state.col);
    }
};

Sudoku.prototype.move_active_down = function () {
    if (this.state.row < 9) {
        this.set_active(this.state.row + 1, this.state.col);
    }
};

Sudoku.prototype.handle_resize = function () {
    $("div.thermometer").remove();
    if (this.state.puzzle && this.state.puzzle.constraints) {
        for (let i = 0; i < this.state.puzzle.constraints.length; i++) {
            const constraint = this.state.puzzle.constraints[i];
            if (constraint.type === "thermometer") {
                for (let j = 0; j < constraint.params.length; j++) {
                    const thermo = constraint.params[j];
                    this.paint_thermo(thermo, constraint.color);
                }
            }
        }
    }
};

Sudoku.prototype.paint_thermo = function (thermo, color) {
    var parent = $(".sudoku-table");
    var parentRect = parent[0].getBoundingClientRect();
    var thermoDiv = document.createElement("div");
    thermoDiv.className = "thermometer";

    // paint bulb
    var bulbPos = thermo[0];
    var bulbCellId = make_id(bulbPos[0], bulbPos[1]);
    var bulbRect = $("." + bulbCellId)[0].getBoundingClientRect();
    var bulb = document.createElement("div");
    bulb.className = "thermo-bulb";
    bulb.style["background-color"] = color;
    bulb.style.top = bulbRect.y - parentRect.y + bulbRect.height * 0.15;
    bulb.style.left = bulbRect.x - parentRect.x + bulbRect.width * 0.15;
    bulb.style.width = bulbRect.width * 0.7;
    bulb.style.height = bulbRect.height * 0.7;
    thermoDiv.append(bulb);

    var startPos = thermo[0];
    for (var i = 1; i < thermo.length; i++) {
        var endPos = thermo[i];
        // TODO: Paint thermo part
        var startRect = $("." + make_id(startPos[0], startPos[1]))[0].getBoundingClientRect();
        var endRect = $("." + make_id(endPos[0], endPos[1]))[0].getBoundingClientRect();
        var angle = calculate_angle(startRect.x, startRect.y, endRect.x, endRect.y);

        var lineDiv = document.createElement("div");
        lineDiv.className = "thermo-line";
        lineDiv.style["background-color"] = color;
        lineDiv.style["border-color"] = color;
        lineDiv.style.height = startRect.height / 3;
        lineDiv.style.top = startRect.y - parentRect.y + startRect.height * 0.35;
        lineDiv.style.left = startRect.x - parentRect.x + startRect.width * 0.5;
        lineDiv.style.width = calculate_length(startRect.x, startRect.y, endRect.x, endRect.y, endRect.width);
        lineDiv.style.borderRadius = (startRect.height / 4) + "px";
        lineDiv.style.transform = "rotate("+angle+"deg)";
        lineDiv.style["transform-origin"] = "center left";
        thermoDiv.append(lineDiv);

        var dot = document.createElement("div");
        dot.className = "thermo-dot";
        dot.style["background-color"] = color;
        dot.style.top = endRect.y - parentRect.y + endRect.height * 0.35;
        dot.style.left = endRect.x - parentRect.x + endRect.width * 0.35;
        dot.style.width = endRect.width/3;
        dot.style.height = endRect.height/3;
        thermoDiv.append(dot);

        startPos = endPos;
    }
    parent.append(thermoDiv);
};

Sudoku.prototype.paint_cage = function (sum, cells) {
    // TODO Put sum into top-left cell
    // TODO put grid around outer borders of cage
    var cellInfos = cells.map((cell) => [cell, make_id(cell[0], cell[1])]);
    cellInfos.sort();
    var topLevelCell = cellInfos[0];
    console.log(topLevelCell);

    var cellIds = new Set();
    cellInfos.forEach((info) => {
        cellIds.add(info[1]);
    });

    cellInfos.forEach((info) => {
        var row = info[0][0];
        var col = info[0][1];
        var id = info[1];
        var div = $("." + id).find(".inner-cell");
        console.log(id);
        if (!cellIds.has(make_id(row-1, col))) {
            console.log("no top", make_id(row-1, col));
            div.addClass("cage-top");
        }
        if (!cellIds.has(make_id(row+1, col))) {
            console.log("no bottom", make_id(row+1, col));
            div.addClass("cage-bottom");
        }
        if (!cellIds.has(make_id(row, col-1))) {
            console.log("no left", make_id(row, col-1));
            div.addClass("cage-left");
        }
        if (!cellIds.has(make_id(row, col+1))) {
            console.log("no right", make_id(row, col+1));
            div.addClass("cage-right");
        }
    });
};

Sudoku.prototype.load_puzzle_constraint = function (constraint) {
    switch (constraint.type) {
        case "same_digit":
            console.log("TODO Implement " + constraint.type);
            break;
        case "evens":
            console.log("TODO Implement " + constraint.type);
            break;
        case "odds":
            console.log("TODO Implement " + constraint.type);
            break;
        case "thermometer":
            $("div.thermometer").remove();
            constraint.params.forEach(function (thermo) {
                this.paint_thermo(thermo, constraint.color);
            }.bind(this));
            break;
        case "cage":
            constraint.params.forEach(function (cage) {
                this.paint_cage(cage[0], cage[1]);
            }.bind(this));
            break;
        default:
            throw "Unknown puzzle constraint type: " + constraint.type;
    }
};

Sudoku.prototype.load_puzzle = function (oData) {
    this.state.puzzle = oData;
    // set digits
    for (var row = 1; row <= oData.digits.length; row++) {
        for (var col = 1; col <= oData.digits[row - 1].length; col++) {
            var id = make_id(row, col);
            var digit = oData.digits[row - 1][col - 1];
            if (digit != 0) {
                this.state.fixed_digits.set(id, digit);
            }
        }
    }
    // set title, description, author
    if (oData.title) {
        $(".header").text(oData.title);
    }
    if (oData.author) {
        $(".info-author").text(oData.author);
    }
    if (oData.description) {
        $(".info-description").text(oData.description);
    }
    if (oData.constraints) {
        oData.constraints.forEach(function (constraint) {
            this.load_puzzle_constraint(constraint);
        }.bind(this));
    }
    this.render_all();
};
